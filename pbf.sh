#!/bin/bash

if [[ -d /home/$USER/Pictures/pbf ]]; then
  echo "AWESOME! dir [~/Pictures/pbf/] already exists."
else
  mkdir /home/$USER/Pictures/pbf
  echo "dir [~/Pictures/pbf/] has been created successfully!"
fi

if [[ -z $1 ]]; then
  pbfpy
else
  pbfpy $1
fi
